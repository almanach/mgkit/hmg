#!/usr/bin/env perl

use strict;

my $current;
my $vars = {};
my $intree=0;

while (<>) {
  if (/<tree/) {
    $current = $_;
    $vars = {};
    $intree = 1;
    next;
  }
  unless ($intree) {
    print;
    next;
  }  
  $current .= $_;
  if (m{</tree>}) {
    foreach my $var (grep {$vars->{$_}==1} keys %$vars) {
      $current =~ s{^(\s*)(<f name="\S+">\n\s*<var\s+name="\Q$var\E"/>\n\s*</f>)}{$1<!-- $2 -->}msg;
    }
    print $current;
    undef $vars;
    undef $current;
    $intree=0;
    next;
  }
  if (m{<var\s+name="(\S+?)"/>}) {
    $vars->{$1}++;
    next;
  }
  if (/<var\s+name="(\S+?)"/) {
    $vars->{$1} += 2;
    next
  }
}
