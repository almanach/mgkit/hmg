/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2009, 2010, 2011 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  postheader.tag -- Post header to refine :-tag_mode info
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

%%:-tag_features('S','S'{ xarg => _X, control => _C},'S'{ xarg => _X, control => _C}).
%%:-tag_mode('S'{},all,((+),'S'{ mode => (+) }),+,-).
%%:-tag_mode('S'{},all,((+),'S'{ extraction => (+), xarg => (+) }),+,-).
%%:-tag_mode('S'{},all,((+),'S'{ xarg => (+) }),+,-).
%%:-tag_mode('S'{},subst,((+),'S'{ extraction => (+) }),+,-).
%%:-tag_mode('N2'{},all,((+),'N2'{ sat => (+) }),+,-).
%%:-lctag(incise,special,special,[]).
:-lctag(follow_coord_aux,special,special,[]).
:-lctag(follow_coord_aux2,special,special,[]).
:-lctag('28 empty_spunct shallow_auxiliary',special,[]).
:-tag_mode('S'{},subst,((+),'S'{ mode => (+), extraction => (+) }),+,-).

:-tag_mode('V'{},top,((+),'V'{ mode => (+) }),+,-).
:-tag_mode('V'{},bot,((+),'V'{ mode => (+) }),+,-).
:-tag_mode('Infl'{},top,((+),'Infl'{ mode => (+) }),+,-).
:-tag_mode('Infl'{},bot,((+),'Infl'{ mode => (+) }),+,-).
:-tag_mode('S'{},top,((+),'S'{ mode => (+), extraction => (+), sat => (+) }),+,-).
:-tag_mode('S'{},bot,((+),'S'{ mode => (+), extraction => (+), sat => (+) }),+,-).

:-tag_mode('VMod'{},top,((+),'VMod'{ position => (+), cat => (+) }),+,-).
:-tag_mode('VMod'{},bot,((+),'VMod'{ position => (+), cat => (+) }),+,-).


%% incise has a very specific behaviour that break normal lctag computations
:-lctag_safe(incise).
:-tag_mode(incise{},top,((+),incise{ incise_kind => (+), sync => (+) }),+,-).
:-tag_mode(incise{},bot,((+),incise{ incise_kind => (+), sync => (+) }),+,-).

:-adjrestr(det{},left,atmostone).
:-adjrestr(det{},right,atmostone).
:-adjrestr('V1'{},right,atmostone).
:-adjrestr(pro{},right,atmostone).
:-adjrestr(pro{},left,atmostone).

:-adjrestr('ArgComp'{},right,atmostone).

%:-tag_features('PP',X::'PP'{},X).
:-tag_features(adj,adj{ aux_req => A, pre_det => P },adj{ aux_req => A, pre_det => P }).
:-tag_features(adv,adv{ chunk_type => C },adv{ chunk_type => C }).
:-tag_features(pres,pres{ chunk_type => C },pres{ chunk_type => C }).

