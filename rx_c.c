/** 
 * ----------------------------------------------------------------
 * $Id: rx_c.c 1903 2013-01-07 22:01:02Z clerger $
 * Copyright (C) 2004, 2005, 2006, 2009, 2010, 2011, 2012, 2013 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  \file  rx.c 
 *  \brief Regular Expression API for DyALog
 *
 * ----------------------------------------------------------------
 * Description
 * Inspired from rx package from Van Noord
 * http://odur.let.rug.nl/~vannoord/prolog-rx/
 * ----------------------------------------------------------------
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <regex.h>
#include "libdyalog.h"
#include "builtins.h"


#ifndef HAVE_STRNDUP

size_t
my_strnlen (const char *string, size_t maxlen)
{
  const char *end = memchr (string, '\0', maxlen);
  return end ? (size_t) (end - string) : maxlen;
}

char *
strndup (s, n)
     const char *s;
     size_t n;
{
  size_t len = my_strnlen (s, n);
  char *new = malloc (len + 1);

  if (new == NULL)
    return NULL;

  new[len] = '\0';
  return (char *) memcpy (new, s, len);
}

#endif

Bool is_capitalized (const char *s) 
{
    char c = s[0];
    return c != tolower(c);
}

Bool is_number (const char *s) 
{
    char *endptr;
    double val;
    errno = 0;
    val = strtod(s,&endptr);

    if ( errno
         || endptr == s
             // || *endptr != '\0'
         )
        Fail;
    
    Succeed;
}

char *
lowercase( const char * s) 
{
    unsigned long n = 1+strlen(s);
    char * r = (char *) GC_MALLOC_ATOMIC(n);
    unsigned long i = 0;
    for(; i < n; i++) {
        r[i] = tolower(s[i]);
    }
    return r;
}

char *
uppercase( const char * s) 
{
    unsigned long n = 1+strlen(s);
    char * r = (char *) GC_MALLOC_ATOMIC(n);
    unsigned long i = 0;
    for(; i < n; i++) {
        r[i] = toupper(s[i]);
    }
    return r;
}

char *
n_suffix( const char * s, unsigned int m) 
{
    unsigned long n = strlen(s);
    if (n > m) {
        char * r = (char *) GC_MALLOC_ATOMIC(m+1);
        unsigned long i = 0;
        for(; i < m; i++) {
            r[i] = s[n-m+i];
        }
        r[m] = 0;
        return r;
    } else {
        return s;
    }
}


regex_t *regcomp_pl(const char *s1,
                    const int reg_extended,
                    const int reg_newline,
                    const int reg_icase,
                    const int reg_nosub)
{
  regex_t r;
  int err;
  int flags=0;
  regex_t *t = (regex_t *)malloc(sizeof(r));
  if (reg_extended) flags=flags|REG_EXTENDED;
  if (reg_newline)  flags=flags|REG_NEWLINE;
  if (reg_icase)    flags=flags|REG_ICASE;
  if (reg_nosub)    flags=flags|REG_NOSUB;

  err = regcomp (t, s1, flags);
  if (err)
    {
      char errmsg[100];
      regerror (err, t, errmsg, 100);
      puts (errmsg);
      regfree (t);
      free (t);
      dyalog_error_printf( "error: %s", errmsg );
      return 0;
    }
  else
    return t;
}

void regfree_pl(regex_t *r) {
  regfree(r);
  free(r);
}

Bool
regexec_pl(char *s2,             /* string to be matched    */
           const int reg_notbol,
           const int reg_noteol,
           regex_t *r,           /* compiled expression     */
           const int need_subs,  /* return matched subs ?
                                  * should be 0 if r compiled
                                  * with REG_NOSUB
                                  */
           sfol_t res
           ){
    int err;
    int flags=0;
    int subs=(*r).re_nsub;
    regmatch_t regs[subs+1];
    char errmsg[100];

    if (reg_notbol) flags=flags|REG_NOTBOL;
    if (reg_noteol) flags=flags|REG_NOTEOL;
    err = regexec (r, s2, subs+1, regs, flags);
    switch (err) {
        case 0:
        {
            fol_t t = FOLNIL;
            if (need_subs) {
                int i;
                for (i=subs;i>0;i--){
                    t = Create_Pair(Create_Atom((char *)strndup(&s2[regs[i].rm_so],
                                                                regs[i].rm_eo-regs[i].rm_so)
                                                ),
                                    t);
//                    dyalog_printf("Get Subs Exp %d %d => %&f\n",subs,i,t);
                }
            }
            return sfol_unify( t, Key0, res->t, res->k );
        }
        
        case REG_NOMATCH:
        {
            Fail;
        }
        
        default:
        {
            regerror (err, r, errmsg, 100);
            dyalog_error_printf("Error in regular expression %s",errmsg);
            Fail;
        }
    }
}

static fol_t
strtok_aux(char * s,
           const char *delims,
           fol_t last
           ) 
{
    char *token = strtok(s,delims);
    if (!token) {
        return last;
    } else {
        return Create_Pair(Create_Atom((char *)strdup(token)),strtok_aux(NULL,delims,last));
    }
}

Bool
DyALog_strtok( const char *s,
               const char *delims,
               sfol_t last,
               sfol_t res ) 
{
    char s2[strlen(s)+1];
    strcpy(s2,s);
    return sfol_unify( strtok_aux( s2, delims, last->t), last->k, res->t, res->k);
}

char *
clean_apostrophe(const char *s) 
{
    unsigned long n = 1+strlen(s);
    char *r = (char *)GC_MALLOC_ATOMIC(n);
    unsigned long i=0;
    unsigned long j=0;
    for(; i < n; i++ ) {
        if (!(i && s[i] == ' ' && s[i-1] == '\'')) {
            r[j++] = s[i];
        }
    }
    return r;
}


inline static
fol_t deriv2number (const char *s)
{
    const char *d=s;
    for(; *d ; ) {
        if (*d++ == 'd')
            return DFOLINT(strtoul(d,NULL,10));
    }
    return Create_Atom(s);
}

//static fol_t table[6000];
/* iterative version for creating a bounded token list */
static fol_t
strtok_bounded_alt(char * s,
                   const char *delims,
                   fol_t res,
                   unsigned long bound
                   ) 
{
    fol_t table[bound];
    long i = 0;
    char *s2 = s;
    char *token;
    for( ; (token = strtok(s2,delims)) && (i < bound) ; s2 = NULL) {
        long n;
        long stop;
        char *endptr;
        const char *d=token;
//        printf("token %s\n",token);
        for(; *d ; ) {
            if (*d++ == 'd')
                n = strtoul(d,&endptr,10);
        }
        stop = n+1;
//        printf("n=%d endptr=%x\n",n,endptr);
        if (endptr && *endptr == ':')
            stop += strtoul(endptr+1,NULL,10);
//        printf("n=%d stop=%d\n",n,stop);
        for(; (n < stop) && (i < bound); n++) {
            table[i++] = DFOLINT(n);
        }
    }        
    for(; i > 0; res = Create_Pair(table[--i],res));
    return res;
}

Bool
DyALog_bounded_strtok( const char *s,
                       const char *delims,
                       sfol_t last,
                       sfol_t res,
                       unsigned long bound
                       ) 
{
    char s2[strlen(s)+1];
    strcpy(s2,s);
    return sfol_unify( strtok_bounded_alt( s2, delims, last->t, bound), last->k, res->t, res->k);
}

fol_t
DerivHandler(char *s){
    return strtok_bounded_alt(s," ",FOLNIL,10000);
}

fol_t
ListHandler( char *s) 
{
    return strtok_aux(s," ",FOLNIL);
}

fol_t
SpanHandler(char *s)
{
    fol_t res = FOLNIL;
    fol_t table[4];
    long i = 0;
    char *s2 = s;
    char *token;
    for( ; (token = strtok(s2," ")) && (i < 4) ; s2 = NULL) {
        table[i++] = DFOLINT(strtoul(token,NULL,10));
    }
    for(; i > 0; res = Create_Pair(table[--i],res));
    return res;
}

char *UTF8_to_XMLLatin1(char *);

fol_t
LatinHandler(char *s) 
{
    return Create_Atom(UTF8_to_XMLLatin1(s));
}

fol_t
PositionHandler(char *s)
{
    return DFOLINT(strtoul(s,NULL,10));
}

void * GetAttrHandler (long x) 
{
    switch (x) {
        case 1:
            return (void *) &DerivHandler;
            break;
        case 2:
            return (void *) &ListHandler;
            break;
        case 3:
            return (void *) &SpanHandler;
            break;
        case 4:
            return (void *) &LatinHandler;
            break;
        case 5:
            return (void *) &PositionHandler;
            break;
        default:
            return NULL;
    }
}


static char * strtok_buffer;
static unsigned long strtok_n=0;

Bool
DyALog_choice_strtok( const char *s,
                      const char *delims,
                      char **res
                      ) 
{
    int n = Get_Choice_Counter();
    char *token;
    char **ptr = Get_Choice_Buffer(char **);
    if (n==0) {
        unsigned long l = strlen(s)+1;
        if (l > strtok_n) {
            strtok_n = 2*l;
            strtok_buffer = (char *) GC_MALLOC_ATOMIC(strtok_n);
        }
        strcpy(strtok_buffer,s);
        token = strtok_r(strtok_buffer,delims,ptr);
    } else {
        token = strtok_r(NULL,delims,ptr);
    }
    if (!token) {
        No_More_Choice();
        Fail;
    } else {
        *res = strdup(token);
        Succeed;
    }
}

static fol_t
sfol_full_copy( fol_t t, fkey_t Sk(t) )
{
    Deref(t);

    if (FOL_GROUNDP(t)) {
        return t;
    } else if (FOL_DEREFP(t)) {
        dyalog_printf("** pb variable in sfol_full_copy %&f\n",t);
        return t;
    } else {
        unsigned long arity = FOLCMP_ARITY( t );
        fol_t *arg = &FOLCMP_REF(t,1);
        fol_t *stop = arg + arity;

        FOLCMP_WRITE_START( FOLCMP_FUNCTOR( t ), arity );
    
        for (; arg < stop ;)
            FOLCMP_WRITE( sfol_full_copy( *arg++, Sk(t)) );
    
        return FOLCMP_WRITE_STOP;
    }
}


Bool
DyALog_Copy( sfol_t src, sfol_t dest ) 
{
    return Unify(dest->t,dest->k,sfol_full_copy(src->t,src->k),Key0);
}

typedef struct wlist 
{
    long w;
    fol_t dstruct;
    struct wlist *next;
} *wlist_t;

typedef struct llist
{
    unsigned long length;
    wlist_t wlist;
    struct llist *next;
} *llist_t;

static llist_t dstruct_list = NULL;

void
Easyforest_Reset_DStruct() 
{
    dstruct_list = NULL;
}


void
Easyforest_Add_DStruct ( const unsigned long length, const long w, sfol_t dstruct ) 
{
    llist_t *ll = &dstruct_list;
    llist_t lcell;
    wlist_t wcell = GC_MALLOC(sizeof(struct wlist));
    wcell->w = w;
    wcell->dstruct = sfol_full_copy(dstruct->t,dstruct->k);
    for(; *ll ; ll = &((*ll)->next)) {
        if ((*ll)->length == length) {
            wlist_t *wl = &((*ll)->wlist);
            for(; *wl && ((*wl)->w > w); wl = &((*wl)->next));
            wcell->next = *wl;
            *wl = wcell;
            return;
        } else if ((*ll)->length < length)
            break;
    }
    lcell = GC_MALLOC(sizeof(struct llist));
    wcell->next = NULL;
    lcell->length = length;
    lcell->wlist = wcell;
    lcell->next = *ll;
    *ll = lcell;
}

Bool
Easyforest_Get_DStruct (sfol_t dstruct ) 
{
    int n = Get_Choice_Counter();
    llist_t *ll = Get_Choice_Buffer(llist_t *);
    wlist_t *wl = Get_Choice_Buffer(wlist_t *)+1;
    if (n==0) {
        *ll = dstruct_list;
        *wl = NULL;
    }
    for(; !*wl && *ll; *wl = (*ll)->wlist, *ll = (*ll)->next);
    if (*wl) {
        fol_t t = (*wl)->dstruct;
        *wl = (*wl)->next;
        return Unify(t,Key0,dstruct->t,dstruct->k);
    }
    No_More_Choice();
    Fail;
}

static
fol_t fast_append(SP(a), fol_t b) 
{
    Deref(a);
    if (FOLNILP(a))
        return b;
    else
        return Create_Pair(FOLPAIR_CAR(a),fast_append(FOLPAIR_CDR(a),Sk(a),b));
}


Bool
Easyforest_Fast_Append(sfol_t a, sfol_t b, sfol_t c) 
{
    SFOL_Deref(a);
    return Unify(fast_append(a->t,a->k,sfol_full_copy(b->t,b->k)),Key0,c->t,c->k);
}

typedef struct dlist 
{
    long w;
    fol_t oid;
    fol_t dinfo;
    struct dlist *next;
} *dlist_t;

dlist_t *
Easyforest_DList_Init () 
{
    dlist_t * dlist = (dlist_t *) PUSH_TRAIL_BLOCK( sizeof( dlist_t ));
    *dlist = NULL;
    return dlist;
}

void
Easyforest_DList_Add (dlist_t *dlist, const long w,
                      sfol_t oid,
                       sfol_t dinfo) 
{
    dlist_t dcell;
    SFOL_Deref(oid);
    for(; *dlist; dlist = &((*dlist)->next)) {
        if ((*dlist)->oid == oid->t) {
            if ((*dlist)->w <= w) {
                (*dlist)->w = w;
                (*dlist)->dinfo = sfol_full_copy(dinfo->t,dinfo->k);
            }
            return;
        }
    }
    *dlist = dcell = (dlist_t) GC_MALLOC(sizeof(struct dlist));
    dcell->next = NULL;
    dcell->w = w;
    dcell->oid = oid->t;
    dcell->dinfo = sfol_full_copy(dinfo->t,dinfo->k);
}


Bool
Easyforest_DList_Get(const dlist_t *dlist, long *xw, const sfol_t t) 
{
    fol_t res=FOLNIL;
    long w = 0;
    dlist_t d = *dlist;
    for(; d; d = d->next) {
        w += d->w;
        res = Create_Pair(d->dinfo,res);
    }
    *xw = w;
    return Unify(res,Key0,t->t,t->k);
}

typedef struct rx_deriv 
{
    fol_t node;
    fol_t op;
    fol_t ht;
    fol_t edges;
} *rx_deriv_t;

rx_deriv_t
Deriv_New(sfol_t node) 
{
    rx_deriv_t deriv = GC_MALLOC(sizeof(struct rx_deriv));
    deriv->node = sfol_full_copy(node->t,node->k);
    deriv->edges = FOLNIL;
    deriv->op = FOLNIL;
    deriv->ht = FOLNIL;
    return deriv;
}

Bool
Deriv_Set_Op(rx_deriv_t deriv,sfol_t op, sfol_t node) 
{
    deriv->op=sfol_full_copy(op->t,op->k);
    return Unify(deriv->node,Key0,node->t,node->k);
}

void
Deriv_Set_Hypertag(rx_deriv_t deriv, sfol_t ht)
{
    deriv->ht=sfol_full_copy(ht->t,ht->k);
}

void
Deriv_Add_Edge(rx_deriv_t deriv, sfol_t e) 
{
    deriv->edges= Create_Pair(sfol_full_copy(e->t,e->k),deriv->edges); 
}

Bool
Deriv_Info(rx_deriv_t deriv, sfol_t node, sfol_t op, sfol_t ht) 
{
    return Unify(deriv->node,Key0,node->t,node->k)
        && Unify(deriv->op,Key0,op->t,op->k)
        && Unify(deriv->ht,Key0,ht->t,ht->k);
}

Bool
Deriv_Get_Node(rx_deriv_t deriv, sfol_t node) 
{
    return Unify(deriv->node,Key0,node->t,node->k);
}

Bool
Deriv_Get_Node_And_Edges(rx_deriv_t deriv, sfol_t node, sfol_t edges) 
{
    return Unify(deriv->node,Key0,node->t,node->k)
        && Unify(deriv->edges,Key0,edges->t,edges->k);
}

Bool
Deriv_Get_Hypertag(rx_deriv_t deriv, sfol_t ht) 
{
    return Unify(deriv->ht,Key0,ht->t,ht->k);
}

/* Bool */
/* Deriv_Get_Op(rx_deriv_t deriv, sfol_t op)  */
/* { */
/*     return Unify(deriv->op,Key0,op->t,op->k); */
/* } */

Bool
Deriv_Edges(rx_deriv_t deriv,sfol_t edges) 
{
    return Unify(deriv->edges,Key0,edges->t,edges->k);
}
    
