## My completion file for adding missing features to words
## beware of tabs as separator (not blanks)

## *** Note: this file should be converted in UTF8
    
## souvent	adv	[adv_kind=freq]
autant	adv	[adv_kind=equalizer]
davantage	adv	[adv_kind=equalizer]
aussi	adv	[adv_kind=equalizer]
##assez	adv	[adv_kind=quantity]
##trop	adv	[adv_kind=quantity]
##suffisamment	adv	[adv_kind=quantity]

voici	v	[mode=imperative]
voil�	v	[mode=imperative]    
revoici	v	[mode=imperative]
revoil�	v	[mode=imperative]
Voici	v	[mode=imperative]
Voil�	v	[mode=imperative]    
Revoici	v	[mode=imperative]
Revoil�	v	[mode=imperative]
�tre	v	[�tre= +]

    
printemps	nc	[@time]
�t�	nc	[@time]
automne	nc	[@time]
hiver	nc	[@time]

si�cle	nc	[@time]
mill�naire	nc	[@time]

## il habita quelques temps ....
temps	nc	[@time]

fort	adv	[adv_kind=intensive|-]
tout	adv	[adv_kind=intensive|-]
� peine	adv	[adv_kind=intensive|-|modnc]

    
m�me	adj	[degree=+]

l�	adv	[adv_kind=loc]
ici	adv	[adv_kind=loc]

hier	adv	[adv_kind=time]
demain	adv	[adv_kind=time]
avant-hier	adv	[adv_kind=time]
aujourd'hui	adv	[adv_kind=time]
jadis	adv	[adv_kind=time]
jusqu'alors	adv	[adv_kind=time]
tout � l'heure	adv	[adv_kind=time]
un peu plus tard	adv	[adv_kind=time]
un peu plus t�t	adv	[adv_kind=time]
� ce moment-l�	adv	[adv_kind=time]

y compris	prep	[pcas=prepmod]
non compris	prep	[pcas=prepmod]
sauf	prep	[pcas=prepmod]
except�	prep	[pcas=prepmod]
non compris	prep	[pcas=prepmod]
voire	prep	[pcas=prepmod]
hormis	prep	[pcas=prepmod]

ci-dessus	adv	[adv_kind=modnc]
ci-dessous	adv	[adv_kind=modnc]
ci-apr�s	adv	[adv_kind=modnc]
ci-contre	adv	[adv_kind=modnc]
supra	adv	[adv_kind=modnc]
infra	adv	[adv_kind=modnc]
surtout	adv	[adv_kind=modnc]

m�me	adv	[adv_kind=equalizer|modnc]
loin	adv	[adv_kind=loc|qmod]
pr�s	adv	[adv_kind=loc|qmod]
proche	adv	[adv_kind=loc|qmod]

_NUMBER	det	[countable=+]
un	det	[countable=+]
quelque	det	[countable=+]

quoi?	pri	[case=acc|-]

main	nc	[semtype=bodypart]
t�te	nc	[semtype=bodypart]
genou	nc	[semtype=bodypart]
face	nc	[semtype=bodypart]
pied	nc	[semtype=bodypart]
oeil	nc	[semtype=bodypart]
dos	nc	[semtype=bodypart]
fesse	nc	[semtype=bodypart]
menton	nc	[semtype=bodypart]
�paule	nc	[semtype=bodypart]
regard	nc	[semtype=bodypart]
bouche	nc	[semtype=bodypart]
sourcil	nc	[semtype=bodypart]
cheveu	nc	[semtype=bodypart]
coude	nc	[semtype=bodypart]

## want to overwrite information
parvenir	v	+[aux-req=�tre|avoir]
achever	v	+[aux-req=�tre|avoir]

voici	prep	[pcas=tps]
voil�	prep	[pcas=tps]

outre-Rhin	adv	[adv_kind=loc]
outre-Manche	adv	[adv_kind=loc]
outre-Atlantique	adv	[adv_kind=loc]
outre-mer	adv	[adv_kind=loc]
outre-tombe	adv	[adv_kind=loc]
ailleurs	adv	[adv_kind=loc]
l�-bas	adv	[adv_kind=loc]
ici-bas	adv	[adv_kind=loc]
au centre	adv	[adv_kind=loc]
au dedans	adv	[adv_kind=loc]
au dehors	adv	[adv_kind=loc]
au diable Vauvert	adv	[adv_kind=loc]
au-dehors	adv	[adv_kind=loc]
au-dedans	adv	[adv_kind=loc]
au-dessous	adv	[adv_kind=loc]
au-dessus	adv	[adv_kind=loc]
au-del�	adv	[adv_kind=loc]
au-devant	adv	[adv_kind=loc]
en arri�re	adv	[adv_kind=loc]
en avant	adv	[adv_kind=loc]
en bas	adv	[adv_kind=loc]
en dedans	adv	[adv_kind=loc]
en dessous	adv	[adv_kind=loc]
en dessus	adv	[adv_kind=loc]
l�-dedans	adv	[adv_kind=loc]
l�-dessous	adv	[adv_kind=loc]
l�-dessus	adv	[adv_kind=loc]
l�-derri�re	adv	[adv_kind=loc]

par-dedans	adv	[adv_kind=loc]
par-dessous	adv	[adv_kind=loc]
par-dessus	adv	[adv_kind=loc]
par-derri�re	adv	[adv_kind=loc]

urbi et orbi	adv	[adv_kind=loc]
orbi	adv	[adv_kind=loc]
urbi	adv	[adv_kind=loc]

� droite	adv	[adv_kind=loc]
� gauche	adv	[adv_kind=loc]

## apply to lemma !
tel	adj	[proadj= +]

de plus en plus	adv	[adv_kind=intensive]
de moins en moins	adv	[adv_kind=intensive]

## not sure of the following
encore	adv	+[adv_kind=intensive|-|modnc|modpro]

## want to overwrite info
que	prel	+[case=acc|quemod]

plus	adv	+[adv_kind=intensive|qmod]
moins	adv	+[adv_kind=intensive|qmod]

aussi bien	adv	[adv_kind=equalizer]

� ce point	adv	[adv_kind=intensive]

saison	nc	[@time]
