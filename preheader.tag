/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id: preheader.tag 1855 2012-10-31 13:11:47Z clerger $
 * Copyright (C) 2001, 2004, 2008, 2009, 2010, 2011, 2012 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  preheader.tag -- Header file for TAG Grammar
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include_unless_def(defheader).

:-require 'tag_generic.pl'.

:-features( tag_tree, [ family, name, tree ] ).
:-features( tag_anchor, [name, coanchors, equations ] ).

:-tag_mode(_,all,[+|-],+,-).

:-features( lexicon, [lex,cat,ht,top] ).

:-extensional lexicon{}.

%% Need to be on a single line for the extraction to build small_header.tag
%% the extractor should be made more clever in some future
:-finite_set(ponct,[';','.','!','?','...','?!',':',',','"','(',')','[',']','�','�','etc.','!?','-','*','_','+','_SMILEY','_SENT_BOUND','!!!','/']).

:-subset(initponct,ponct['*','-','_','+']).

%%:-tagfilter(Name^Left^Right^NT^Top^(recorded(robust) xor ok_tree(Name,Left))).

:-tagfilter(Name^Left^Right^NT^Top^(check_ok_tree(Name,Left))).

%%:-tagfilter_cat(Cat^Mode^Left^Right^(check_ok_cat(Cat,Mode,Left))).
:-tagfilter_cat(Cat^Mode^Left^Right^(check_ok_cat(Cat,Mode,Left,Right))).

%% Optimize multiple tig tree adjoining because they don't cover empty spans
:-no_empty_tig_trees.

%% Optimize kleene loops because their body don't cover empty span
:-no_empty_kleene_body.

%% Max boundary on the number of tig adjoinings on each side
:-max_tig_adj(5).

%% New correction mechanism
%% :-tag_corrections.

%% Use variance instead of subsumption
%% seems to be a bit slower than subsumption
%% :-subsumption(tag(variance)).

%% special predicates
%% :-light_tabular(tag(start/0)).
%% :-light_tabular(tag(end/0)).

:-tag_terminal_cat(v).
:-tag_terminal_cat(aux).
:-tag_terminal_cat(nc).
:-tag_terminal_cat(adv).
:-tag_terminal_cat(advneg).
:-tag_terminal_cat(adj).
:-tag_terminal_cat(np).
:-tag_terminal_cat(prep).
:-tag_terminal_cat(pri).
:-tag_terminal_cat(prel).
:-tag_terminal_cat(pro).
%%:-tag_terminal_cat(quantity).
%%:-tag_terminal_cat(supermod).
%%:-tag_terminal_cat(det).
:-tag_terminal_cat(title).
:-tag_terminal_cat(number).
:-tag_terminal_cat(ncpred).
%:-tag_terminal_cat(ncpred2).
:-tag_terminal_cat(coo).
:-tag_terminal_cat(csu).

:-tag_subst_cat('PP',[prep]).
:-tag_subst_cat('CS',[csu]).
:-tag_subst_cat('adjP',[adj]).
%% :-tag_subst_cat('det',[det,predet,number]).

%% for exotic handlers

%% To be able to meta-call all TAG non-terminals
:-public(tag(_)).

%% The directive
:-tag_custom_subst(my_subst_handler).

%% The user-defined TAG application has to be a prolog predicate with arity 5
%% it could be a tabular or light_tabular, but that would leave traces in the output forest
%% it can't be rec_prolog or std_prolog because it calls tabulated non-terminals
:-prolog my_subst_handler/5.

%% About proba
:-tag_use_proba.
:-extensional proba/2.
