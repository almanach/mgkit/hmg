## My completion file for adding missing features to words
## beware of tabs as separator (not blanks)

## *** Note: this file should be converted in UTF8
    
## souvent	adv	[adv_kind=freq]

@adj sigmo�de +s + +s
@adj lin�aire +s + +s
@adj ombelliforme +s + +s
@adj bifurqu� +s +e +es
@adj polygame +s + +s
@adj distique +s + +s
##@adj appr�me +s + +s
@adj voyant +s +e +es
@adj minuscule +s + +s
@adj lisse +s + +s
@adj rampant +s +e +es
@adj pubescent +s +e +es
@adj t�tram�re +s + +s
@adj pentam�re +s + +s
@adj terminal terminaux terminale terminales
@adj violet +s +te +tes
@adj oblique  +s + +s
@adj r�ticul� +s +e +es
@adj super +s + +s

@adj structuraliste +s + +s

@adj tomodensitom�trique +s + +s

basai	adj	[pred="basai",cat=adj,@s]	basai____1	Default

@nc stylopode +s @
@nc staminode +s @
@nc testa +s @
@nc m�ricarpe +s @m
@nc exocarpe +s @m
@nc convergente +s @f
@nc pistillode +s @

@nc trilogue +s @m

Roms	np	[cat=np,@p]	Rom	Default

## @nc dignitaire +s @


_DIMENSION	np	[cat=np,@p]	_DIMENSION	Default

dens�ment	adv	[]	dens�ment____20	Default

##ce		pro	[pred="ce____3",number=sg]	ce_____3	Default


##comment		pri	[pred="commentComp?_____1",case= comp]	commentComp?_____1	Default 
quel		pri	[pred="quel?_____1",case= comp,@ms]	quel?_____1	Default 
quelle		pri	[pred="quel?_____1",case= comp,@fs]	quel?_____1	Default 
quels		pri	[pred="quel?_____1",case= comp,@mp]	quel?_____1	Default 
quelles		pri	[pred="quel?_____1",case= comp,@fp]	quel?_____1	Default 
qui		pri	[pred="quiComp?_____1",case= comp]	quiComp?_____1	Default
que		pri	[pred="queComp?_____1",case= comp]	queComp?_____1	Default 
qu'		pri	[pred="queComp?_____1",case= comp]	queComp?_____1	Default 
lequel		pri	[pred="lequelComp?_____1",case= comp,@ms]	lequelComp?_____1	Default 
lesquels	pri	[pred="lequelComp?_____1",case= comp,@mp]	lequelComp?_____1	Default 
laquelle	pri	[pred="lequelComp?_____1",case= comp,@fs]	lequelComp?_____1	Default 
lesquelles	pri	[pred="lequelComp?_____1",case= comp,@fp]	lequelComp?_____1	Default 



lequel		pri	[pred="lequel?_____1",case=nom|acc,@ms]	quel?_____1	Default 
laquelle		pri	[pred="lequel?_____1",case=nom|acc,@fs]	quel?_____1	Default 
lesquels		pri	[pred="lequel?_____1",case=nom|acc,@mp]	quel?_____1	Default 
lesquelles		pri	[pred="lequel?_____1",case=nom|acc,@fp]	quel?_____1	Default 

proto-_		advPref	[pred="proto-______2<Suj:(sn)>",cat=adv]	proto-______2	Default

## Un	det	[det=+,define=-,@ms]	un_____1	Default	ms
## Un	pro	[pred="un_____1<Objde:(de-sn)>",define=-,@ms]	un_____1	Default	ms

pas encore	advneg	[pred="pas encore_____1",cat=advneg]	pas encore_____1	Default 
pr�s de	adv	[pred="pr�s de_____1",cat=adv,adv_kind=modnc]	pr�s de_____1	Default
pr�s d"	adv	[pred="pr�s de_____1",cat=adv,adv_kind=modnc]	pr�s de_____1	Default
:	poncts	[]	:_____2	Default	%default
Wikip�dia	np	[cat=np]	Wikip�dia_____1	Default
DGSE	np	[cat=np,@fs]	DGSE_____1	Default
DST	np	[cat=np,@fs]	DST_____1	Default

eux-m�mes	100	xpro	[pred="lui-m�me_____2",@3mp]	lui-m�me_____1	Default	%default
elles-m�mes	100	xpro	[pred="lui-m�me_____2",@3fp]	lui-m�me_____1	Default	%default
lui-m�me	100	xpro	[pred="lui-m�me_____2",@3ms]	lui-m�me_____1	Default	%default
elle-m�me	100	xpro	[pred="lui-m�me_____2",@3fs]	lui-m�me_____1	Default	%default


## comme det: divers diff�rent
## Quid

Quid	np	[cat=np]	Quid_____1	Default
Gaza	np	[cat=np]	Gaza_____1	Default
Fermat	np	[cat=np]	Gaza_____1	Default
Dinant	np	[cat=np]	Gaza_____1	Default
Octave	np	[cat=np,@s]	Gaza_____1	Default
Martial	np	[cat=np,@ms]	Gaza_____1	Default
Pluton	100	np	[pred="Pluton",cat=np,@ms]	Pluton_____1	Default	ms	 %default

JO	100	np	[pred="Journal_Officiel",cat=np,@ms]	Journal Officiel_____1	Default	ms	%default

@nc finalisation +s @f
@nc d�chetterie +s @f
@nc commetant +s @m
@nc mod�lisme +s @m
@nc boite +s @f
@nc conscrit +s @m
@nc shogunat +s @m
@nc blasonnement +s @m
@nc r�am�nagement +s @m
@nc babouschka + @f
@nc cr�nelage +s @m
@nc d�sossage +s @m
@nc requ�rant +s @m
@nc requ�rante +s @f
@nc v�rificateur +s @m
@nc r�prouv� +s @m
@nc r�prouv�e +s @m
@nc d�put�e +s @f
@nc quartet +s @m
@nc phtalate +s @f
@nc constitutionalisme +s @m
@nc constitutionalisation +s @f
@nc hooliganisme +s @m
@nc taliban + @m
@nc lobbyiste +s @
@nc reculons + @m
@nc chromatographe +s @m
@nc ovoproduit +s @m
@nc multilinguisme +s @m
@nc rembours + @m

@adj nitrique +s + +s
@adj massique +s + +s
@adj proc�dural proc�duraux proc�durale proc�durales
@adj notifiant +s +e +es
@adj natif natifs native natives
@adj tier tiers tierce tierces
@adj m�solithique +s + +s
@adj naufrag� +s +e +es
@adj fr�quentiel +s +le +les
@adj antidopage +s + +s
@adj juridictionel +s +le +les

@adj rus� +s +e +es

chut	pres	[pred="chut_____1"]	chut_____1	Default

divers	100	det	[define=-,det=+,@m]	divers_____1	Default m	%default
diverse	100	det	[define=-,det=+,@fs]	divers_____1	Default fs	%default
diverses	100	det	[define=-,det=+,@fp]	divers_____1	Default fp	%default

diff�rent	100	det	[define=-,det=+,@ms]	diff�rent_____1	Default ms	%default
diff�rents	100	det	[define=-,det=+,@mp]	diff�rent_____1	Default mp	%default
diff�rente	100	det	[define=-,det=+,@fs]	diff�rent_____1	Default fs	%default
diff�rentes	100	det	[define=-,det=+,@fp]	diff�rent_____1	Default fp	%default

## Construction en "X de" comme det

peu	100	predet	[pred="peu_____1",predet_kind=adv,quantity=peu]	peu_____1	Default	%default
beaucoup	100	predet	[pred="beaucoup_____1",predet_kind=adv,quantity=beaucoup]	beaucoup_____1	Default	%default
tant	100	predet	[pred="tant_____1",predet_kind=adv,quantity=beaucoup]	tant_____1	Default	%default
nombre	100	predet	[pred="nombre_____1",predet_kind=nc]	nombre_____1	Default	%default
�norm�ment	100	predet	[pred="�norm�ment_____1",predet_kind=adv,quantity=beaucoup]	�norm�ment_____1	Default	%default
tellement	100	predet	[pred="tellement_____1",predet_kind=adv,quantity=beaucoup]	tellement_____1	Default	%default
immens�ment	100	predet	[pred="immens�ment_____1",predet_kind=adv,quantity=beaucoup]	immens�ment_____1	Default	%default
quantit�	100	predet	[pred="quantit�_____1",predet_kind=nc]	quantit�_____1	Default	%default
plein	100	predet	[pred="plein_____1",predet_kind=adv]	plein_____1	Default	%default
combien	100	predet	[pred="combien_____1",predet_kind=nc]	combien_____1	Default	%default
suffisamment	100	predet	[pred="suffisamment_____1",predet_kind=adv,quantity=beaucoup]	suffisamment_____1	Default	%default
assez	100	predet	[pred="assez_____1",predet_kind=adv,quantity=beaucoup]	assez_____1	Default	%default
trop	100	predet	[pred="trop_____1",predet_kind=adv,quantity=beaucoup]	trop_____1	Default	%default
nul	100	predet	[pred="nul_____1",predet_kind=nc]	nul_____1	Default	%default
plus	100	predet	[pred="plus_____1",predet_kind=adv,quantity=beaucoup]	plus_____1	Default	%default
moins	100	predet	[pred="moins_____1",predet_kind=adv,quantity=beaucoup]	moins_____1	Default	%default
davantage	100	predet	[pred="davantage_____1",predet_kind=adv,quantity=beaucoup]	davantage_____1	Default	%default
## pas is only possible in negative contexte
## il ne veut (pas de) pomme.
## il ne veut presque (pas de) pomme.
## il veut une part avec (pas de) pommes.
pas	100	predet	[pred="pas_____1",predet_kind=advneg]	pas_____1	Default	%default
gu�re	100	predet	[pred="gu�re_____1",predet_kind=advneg]	gu�re_____1	Default	%default

gaiement	100	advm	[pred="gaiement_____1",cat=adv,clive=+]	gaiement_____1	Default	%default

## Actually, voici and voil� are in Lefff as v, but with a special subcat
## No subj
##voici	100	v	[pred="voici_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	voici_____1	Imperative	Y2s	%actif
##revoici	100	v	[pred="revoici_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	revoici_____1	Imperative	Y2s	%actif
##voil�	100	v	[pred="voil�_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	voil�_____1	Imperative	Y2s	%actif
##revoil�	100	v	[pred="revoil�_____1<Suj:cln|sn,Obj:(cla|sn|scompl)>",cat=v,@imperative,@Y2s]	revoil�_____1	Imperative	Y2s	%actif
##quid	100	v	[pred="quid_____1<Suj:cln|sn,Objde:(de-sinf|de-sn)>",cat=v,@imperative,@Y2s]	quid_____1	Imperative	Y2s	%actif

## avec	120	prep	[pred="avec_____1<Obj:(sa|sadv|sn)>",pcas = avec]	avec_____1	Default	%default

## fait	100	v	[pred="faire_____1<Suj:cln|sn,Obj:sinfcaus>",@pers,cat=v,@P3s]	faire_____1	ThirdSing	P3s	%actif

## Tmp: en attendant de completer Lefff
soulign�	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl|qcompl>",@passive,@pers,cat=v,@Kms]	souligner_____1	PastParticiple	Kms	%passif
soulign�e	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl>",@passive,@pers,cat=v,@Kfs]	souligner_____1	PastParticiple	Kfs	%passif
soulign�s	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl>",@passive,@pers,cat=v,@Kmp]	souligner_____1	PastParticiple	Kmp	%passif    
soulign�es	100	v	[pred="souligner_____1<Obl2:(par-sn),Suj:cln|sn|sinf|scompl>",@passive,@pers,cat=v,@Kfp]	souligner_____1	PastParticiple	Kfp	%passif

que	100	csu	[wh=que]	que_____1	Default	%default:
qu'	100	csu	[wh=que]	que_____1	Default	%default
    
_NUMBER	number	[cat=number]	_NUMBER	Default
SHQ:	epsilon	[cat=epsilon]	SHQ:____1	Default
:SHQ	epsilon	[]	:SHQ____1	Default
PRED:	epsilon	[]	PRED:____1	Default
:PRED	epsilon	[]	:PRED____1	Default
AUT CL:	epsilon	[]	AUTCL:____1	Default
:AUT CL	epsilon	[]	:AUTCL____1	Default
AUT:	epsilon	[]	AUT:____1	Default
:AUT	epsilon	[]	:AUT____1	Default
SQ:	epsilon	[cat=epsilon]	SQ:____1	Default
:SQ	epsilon	[]	:SQ____1	Default
STQ:	epsilon	[cat=epsilon]	STQ:____1	Default
:STQ	epsilon	[]	:STQ____1	Default
HQ:	epsilon	[cat=epsilon]	HQ:____1	Default
:HQ	epsilon	[]	:HQ____1	Default
TQ:	epsilon	[cat=epsilon]	TQ:____1	Default
:TQ	epsilon	[]	:TQ____1	Default
DQ:	epsilon	[cat=epsilon]	DQ:____1	Default
:DQ	epsilon	[]	:DQ____1	Default


## Pb with SxPipe: Dieu (incorrectly) corrected into dieu
dieu	100	np	[cat=np,@ms]	Dieu_____1	Default


nul	100	det	[define=-,det=+,@ms]	nul_____1	Default	ms	%default
nuls	100	det	[define=-,det=+,@mp]	nul_____1		Default	mp	%default
nulle	100	det	[define=-,det=+,@fs]	nul_____1		Default	fs	%default
nulles	100	det	[define=-,det=+,@fp]	nul_____1		Default	fp	%default

si	100	adv	[pred="si_____1",adv_kind=intensive,cat=adv]	si_____1	Default	%default
tellement	100	adv	[pred="tellement_____1",adv_kind=intensive,cat=adv]	tellement_____1	Default	%default
plut�t	100	adv	[pred="plut�t_____1",adv_kind=intens,cat=adv]	plut�t_____1	Default	%default

## nouns witch scompl arg
motif	100	nc	[pred="motif_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	motif_____1	Default	ms	%default
souhait	100	nc	[pred="souhait_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	souhait_____1	Default	ms	%default
d�sir	100	nc	[pred="d�sir_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	d�sir_____1	Default	ms	%default
accord	100	nc	[pred="accord_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	accord_____1	Default	ms	%default
constat	100	nc	[pred="constat_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	constat_____1	Default	ms	%default
rappel	100	nc	[pred="rappel_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	rappel_____1	Default	ms	%default
sentiment	100	nc	[pred="sentiment_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	sentiment_____1	Default	ms	%default
doute	100	nc	[pred="doute_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	doute_____1	Default	ms	%default
principe	100	nc	[pred="principe_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	principe_____1	Default	ms	%default

sorte	100	nc	[pred="sorte_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	sorte_____1	Default	fs	%default
envie	100	nc	[pred="envie_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	envie_____1	Default	fs	%default
impression	100	nc	[pred="impression_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	impression_____1	Default	fs	%default
proposition	100	nc	[pred="proposition_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	proposition_____1	Default	fs	%default
croyance	100	nc	[pred="croyance_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	croyance_____1	Default	fs	%default
assurance	100	nc	[pred="assurance_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	assurance_____1	Default	fs	%default
crainte	100	nc	[pred="crainte_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	crainte_____1	Default	fs	%default
inqui�tude	100	nc	[pred="inqui�tude_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	inqui�tude_____1	Default	fs	%default
pens�e	100	nc	[pred="pens�e_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	pens�e_____1	Default	fs	%default
condition	100	nc	[pred="condition_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	condition_____1	Default	fs	%default
annonce	100	nc	[pred="annonce_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	annonce_____1	Default	fs	%default
conclusion	100	nc	[pred="conclusion_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@fs]	conclusion_____1	Default	fs	%default


super	100	advm	[pred="super_____1",cat=adv,adv_kind=intens]	super_____1	Default	%default

@title ma�tre +s @m
@title Me +s @
@title ma�tresse +s @f
@title docteur +s @
@title professeur +s @
@title madame mesdames @f
@title mademoiselle mesdemoiselles @f
@title monsieur messieurs @m
@title Mr MM. @m
@title M. MM. @m
@title M MM @m
@title Mlle Mlles @f
@title Mme Mmes @f
@title Monseigneur Messeigneurs @m
@title Mgr +s @m
@title Dame Dames @f
@title Sir Sirs @m
@title Lady Ladies @f
@title Mess Mess @m
@title R�v�rend R�v @m
@title colonel colonel @m
@title Miss Misses @f
@title baron barons @m

aussi	adv	[pred="aussi_____1",cat=adv,adv_kind=modpro]	aussi_____1	Default
m�me	adv	[pred="m�me_____1",cat=adv,adv_kind=modpro]	m�me_____1	Default
non plus	adv	[pred="nonplus_____1",cat=adv,adv_kind=modpro]	non plus_____1	Default

## plut�t que	prep	[pred="plut�t que_____1<Obj:sn|sa|sadv|sinf>"]	plut�t que_____1	Default	%default

quiconque	100	prel	[pred="quiconque_____1",@pro_nom]	quiconque_____1	Default	%default

## Pb dans Europarl
@redirect cur coeur
@redirect curs coeurs
@redirect nud noeud
@redirect nuds noeuds
@redirect surs soeurs
@redirect consur consoeur
@redirect consurs consoeurs
@redirect belle-sur belle-soeur
@redirect belle-surs belle-soeurs
@redirect demi-sur demi-soeur
@redirect demi-surs demi-soeurs
@redirect chur choeur
@redirect churs choeurs
@redirect contrecur contrecoeur
@redirect contrecurs contrecoeurs
@redirect uvre oeuvre
@redirect uvres oeuvres
@redirect uvrer oeuvrer
@redirect uvrent oeuvrent
@redirect uvr� oeuvr�
@redirect uvr�e oeuvr�e
@redirect uvr�s oeuvr�s
@redirect uvr�es oeuvr�es
@redirect main-d'uvre main-d'oeuvre
@redirect main-d'uvres main-d'oeuvres
@redirect chef-d'uvre chef-d'oeuvre
@redirect chef-d'uvres chef-d'oeuvres
@redirect uf oeuf
@redirect ufs oeufs
@redirect buf boeuf
@redirect bufs boeufs
@redirect manuvre manoeuvre
@redirect manuvres manoeuvres
@redirect manuvrer manoeuvrer
@redirect manuvrent manoeuvrent
@redirect manuvr� manoeuvr�
@redirect manuvr�e manoeuvr�e
@redirect manuvr�s manoeuvr�s
@redirect manuvr�es manoeuvr�es
@redirect �curer �coeurer
@redirect �cure �coeure
@redirect �cures �coeures
@redirect �curent �coeurent
@redirect �cur� �coeur�
@redirect �cur�e �coeur�e
@redirect �cur�s �coeur�s
@redirect �cur�es �coeur�es
@redirect sis situ�
@redirect sise situ�e
@redirect sises situ�es
@redirect r�gne r�gne

## missing in sxpipe/lefff (maybe a bug) 2010/09/28 Eric
@redirect _LOCATION2 _LOCATION
@redirect _PERSON2 _PERSON
@redirect _PERSON_f2 _PERSON_f
@redirect _PERSON_m2 _PERSON_m
@redirect _ORGANIZATION2 _ORGANIZATION
@redirect trad. traduction
@redirect r��d. r��dition
@redirect ed. �diteur

@redirect �v�nement �v�nement

quinzaine	100	nc	[pred="quinzaine",cat=nc,@fs,@time]	quinzaine____1	Default	%default
burqa	100	nc	[pred="burqa",cat=nc,@f]	burqa____1	Default	%default

@nc procureure +s @f
@nc proviseure +s @f

@nc quint� +s @m
@nc trader +s @m
@nc hacker +s @m
@nc rappeur +s @m
@nc rappeuse +s @f
@nc surfeur +s @m
@nc surfeuse +s @f

@nc marqueur +s @m

@adj lampass� +s +e +es
@nc despotat +s @m

@nc polym�rase +s @f
@nc laitier +s @m
@nc vicomt� +s @f

@nc bassidji +s @m 

@nc f�ria +s @f
@nc capitanat +s @m
@nc greffi�re +s @f
@nc gouverneure +s @f
@nc cadencement +s @m
@nc connection +s @f

@adj lorientais + +e +es
@adj rennais    + +e +es
@adj sedanais   + +e +es
@adj perpignanais + +e +es
@adj saskatchewanais + +e +es

@adj �tats-unien  +s +ne +nes
@adj internet + + +

@adj grandissime +s + +s

@nc sprinteur +s @m
@nc paparazzi + @m
@nc sinistrose +s @f

@nc tie-break +s @m

@nc performeur +s @m
@nc performeuse +s @f

@adj za�dite +s + +s
@adj vallonn� +s +e +es

@nc derby derbies @m

@adj celsius + + +

@adj d�sob�isseur +s d�sob�isseuse +s

@nc r�f�rencement +s @m

@adj inarr�table +s + +s

@nc volte-face +s @f

@nc t�l�chargement +s @m
@nc vert�br� +s @m
@nc invert�br� +s @m
@nc naufrag� +s @m
@nc naufrag�e +s @f

@adj proleptique +s + +s
@nc dioxyg�ne +s @m
@adj songha� + + +
@adj burgonde +s + +s
@adj almohade +s + +s
@adj �th�rique +s + +s
@adj fr�quentiel +s +le +les
@adj quadratique +s + +s

@adj diagonalisable +s + +s
@nc d�groupage +s @m

@nc baseballeur +s @m
@nc baseballeuse +s @f

@nc num�rologie +s @f

@nc amine +s @f

@nc alien +s @m

@nc centre-ville +s @m

@adj s�n�galais + +e +es

@adj radiatif +s radiative radiatives
@nc d�senfumage +s @m

@nc diabolisation +s @f
@adj sph�ro�dal sph�ro�daux sph�ro�dale sph�ro�dales
@nc tryptamine +s @f
@adj palois + +e +es
@nc insurgent +s @m
@nc remixe +s @m

@adj quitte +s + +s

chemin	100	cfi	[pred="chemin_____1<Suj:cln|sn>",lightverb=rebrousser]	chemin_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=pr�ter]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=demander]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=chercher]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=qu�rir]	main-forte_____1	Default	%default
main-forte	100	cfi	[pred="main-forte_____1<Suj:cln|sn>",lightverb=requ�rir]	main-forte_____1	Default	%default
gain de cause	100	cfi	[pred="gain de cause_____1<Suj:cln|sn>",lightverb=obtenir]	gain de cause_____1	Default	%default
probl�me	100	cfi	[pred="probl�me_____1<Suj:cln|sn,Obj�:(�-sn|cld)>",lightverb=poser]	probl�me_____1	Default	%default
## gaffe	100	cfi	[pred="gaffe_____1<Suj:cln|sn,Obj�:(�-sn|y|�-sinf|scompl|de-sinf)>",lightverb=faire]	gaffe_____1	Default	%default

pas grand chose	100	pro	[pred="pas grand-chose_____1<Objde:(de-sn|de-sa)>",define=-,@s]	pas grand-chose_____1	Default	%default

cens�	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kms]	censer_____1	PastParticiple	Kms	%actif
cens�e	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfs]	censer_____1	PastParticiple	Kfs	%actif
cens�s	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kmp]	censer_____1	PastParticiple	Kmp	%actif
cens�es	100	v	[pred="censer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfp]	censer_____1	PastParticiple Kfp	%actif

suppos�	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kms]	supposer_____1	PastParticiple	Kms	%actif
suppos�e	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfs]	supposer_____1	PastParticiple	Kfs	%actif
suppos�s	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kmp]	supposer_____1	PastParticiple	Kmp	%actif
suppos�es	100	v	[pred="supposer_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfp]	supposer_____1	PastParticiple Kfp	%actif

r�put�	100	v	[pred="r�puter_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kms]	r�puter_____1	PastParticiple	Kms	%actif
r�put�e	100	v	[pred="r�puter_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfs]	r�puter_____1	PastParticiple	Kfs	%actif
r�put�s	100	v	[pred="r�puter_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kmp]	r�puter_____1	PastParticiple	Kmp	%actif
r�put�es	100	v	[pred="r�puter_____1<Suj:cln|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,@�tre,cat=v,@Kfp]	r�puter_____1	PastParticiple Kfp	%actif

intervenu	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@�tre,cat=v,@Kms]	intervenir_____1	PastParticiple	Kms	%actif
intervenue	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@�tre,cat=v,@Kfs]	intervenir_____1	PastParticiple	Kfs	%actif
intervenus	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@�tre,cat=v,@Kmp]	intervenir_____1	PastParticiple	Kmp	%actif
intervenues	100	v	[pred="intervenir_____1<Suj:cln|sn,Loc:(loc-sn|y)>",@active,@pers,@�tre,cat=v,@Kfp]	intervenir_____1	PastParticiple Kfp	%actif


paru	100	v	[pred="para�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kms]	para�tre_____1	PastParticiple	Kms	%actif
parue	100	v	[pred="para�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfs]	para�tre_____1	PastParticiple	Kfs	%actif
parus	100	v	[pred="para�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kmp]	para�tre_____1	PastParticiple	Kmp	%actif
parues	100	v	[pred="para�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfp]	para�tre_____1	PastParticiple Kfp	%actif

survenu	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kms]	survenir_____1	PastParticiple	Kms	%actif
survenue	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfs]	survenir_____1	PastParticiple	Kfs	%actif
survenus	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kmp]	survenir_____1	PastParticiple	Kmp	%actif
survenues	100	v	[pred="survenir_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfp]	survenir_____1	PastParticiple Kfp	%actif

apparu	100	v	[pred="appara�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kms]	appara�tre_____1	PastApparticiple	Kms	%actif
apparue	100	v	[pred="appara�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfs]	appara�tre_____1	PastApparticiple	Kfs	%actif
apparus	100	v	[pred="appara�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kmp]	appara�tre_____1	PastApparticiple	Kmp	%actif
apparues	100	v	[pred="appara�tre_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfp]	appara�tre_____1	PastApparticiple Kfp	%actif

av�r�	100	v	[pred="av�rer_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kms]	av�rer_____1	PastParticiple	Kms	%actif
av�r�e	100	v	[pred="av�rer_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfs]	av�rer_____1	PastParticiple	Kfs	%actif
av�r�s	100	v	[pred="av�rer_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kmp]	av�rer_____1	PastParticiple	Kmp	%actif
av�r�es	100	v	[pred="av�rer_____1<Suj:cln|sn>",@active,@pers,@�tre,cat=v,@Kfp]	av�rer_____1	PastParticiple Kfp	%actif


d�battu	100	v	[pred="d�battre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kms]	d�battre_____1	PastParticiple	Kms	%passif
d�battue	100	v	[pred="d�battre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfs]	d�battre_____1	PastParticiple	Kfs	%passif
d�battus	100	v	[pred="d�battre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kmp]	d�battre_____1	PastParticiple	Kmp	%passif
d�battues	100	v	[pred="d�battre_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfp]	d�battre_____1	PastParticiple Kfp	%passif

paniqu�	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kms]	paniquer_____1	PastParticiple	Kms	%passif
paniqu�e	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfs]	paniquer_____1	PastParticiple	Kfs	%passif
paniqu�s	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kmp]	paniquer_____1	PastParticiple	Kmp	%passif
paniqu�es	100	v	[pred="paniquer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfp]	paniquer_____1	PastParticiple Kfp	%passif

autoproclam�	100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kms]	autoproclamer_____1	PastParticiple	Kms	%passif
autoproclam�e	100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfs]	autoproclamer_____1	PastParticiple	Kfs	%passif
autoproclam�s	  100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kmp]	autoproclamer_____1	PastParticiple	Kmp	%passif
autoproclam�es	  100	v	[pred="autoproclamer_____1<Obl2:(par-sn),Suj:cln|sn>",@passive,@pers,cat=v,@Kfp]	autoproclamer_____1	PastParticiple Kfp	%passif

surnomm�	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kms]	surnommer_____1	PastParticiple	Kms	%passif
surnomm�e	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kfs]	surnommer_____1	PastParticiple	Kfs	%passif
surnomm�s	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kmp]	surnommer_____1	PastParticiple	Kmp	%passif
surnomm�es	100	v	[pred="surnommer_____1<Obl2:(par-sn),Suj:cln|sn,Att:(sa|sn)>",@passive,@pers,cat=v,@Kfp]	surnommer_____1	PastParticiple Kfp	%passif


modulo	120	prep	[pred="modulo_____1<Obj:(sa|sadv|sn)>",pcas = modulo]	modulo_____1	Default	%default

@redirect commes comme

@nc ataman +s @m
@nc courtine +s @f
@redirect M'sieur Monsieur


avant	120	prep	[pred="avant_____1",pcas = loc]	avant_____1	Default	%default
apr�s	120	prep	[pred="apr�s_____1",pcas = loc]	apr�s_____1	Default	%default    
derri�re	120	prep	[pred="derri�re_____1",pcas = loc]	derri�re_____1	Default	%default

@redirect anni anniversaire

@nc accompagnant +s @m
@nc accompagnante +s @f

moyen	100	cfi	[pred="moyen_____1<Suj:cln|sn,Objde:(de-sinf|scompl)>",lightverb=avoir]	moyen_____1	Default	%default
moyen	100	cfi	[pred="moyen_____1<Suj:cln|sn,Objde:(de-sinf|scompl)>",lightverb=trouver]	moyen_____1	Default	%default

peur	100	cfi	[pred="peur_____1<Suj:cln|sn,Objde:(de-sinf|scompl|de-sn)>",lightverb=avoir]	peur_____1	Default	%default

pr�texte	100	nc	[pred="pr�texte_____1<Obj:scompl,Objde:(de-sinf|de-sn),Obj�:(�-sinf)>",cat=nc,@ms]	pr�texte_____1	Default	ms	%default

trace	100	cfi	[pred="trace_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=porter]	trace_____1	Default	%default
trace	100	cfi	[pred="trace_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=garder]	trace_____1	Default	%default

p�nitence	100	cfi	[pred="p�nitence_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=faire]	p�nitence_____1	Default	%default

garde	100	cfi	[pred="garde_____1<Suj:cln|sn,Objde:de-sn|�-sn>",lightverb=avoir]	garde_____1	Default	%default
raison	100	cfi	[pred="raison_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=avoir]	raison_____1	Default	%default

ripailles	100	cfi	[pred="ripailles_____1<Suj:cln|sn>",lightverb=faire]	ripailles_____1	Default

semblant	100	cfi	[pred="semblant_____1<Suj:cln|sn,Objde:(de-sinf)>",lightverb=faire]	semblant_____1	Default

compte	100	cfi	[pred="compte_____1<Suj:cln|sn,Objde:(de-sn|scompl),Obj�:(�-sn)>",lightverb=rendre]	compte_____1	Default	%default

@nc sicav + @f

# SVP		[cat=nc,@ms]	svp_____1	Default
# STP		[cat=nc,@ms]	svp_____1	Default
# svp		[cat=nc,@ms]	svp_____1	Default
# stp		[cat=nc,@ms]	svp_____1	Default

oh	pres	[pred="oh_____1"]	oh_____1	Default
ho	pres	[pred="ho_____1"]	ho_____1	Default
eh	pres	[pred="eh_____1"]	eh_____1	Default
he	pres	[pred="oh_____1"]	he_____1	Default
ha	pres	[pred="ha_____1"]	ha_____1	Default

pour	120	prep	[pred="pour_____1",pcas = pour]	pour_____1	Default	%default
contre	120	prep	[pred="contre_____1",pcas = loc|contre]	contre_____1	Default	%default

quitte	100	cfi	[pred="quitte_____1<Suj:cln|sn,Obj:(sa|sn),Objde:(de-sn|en)>",lightverb=tenir]	tenir_____1	Default	%default

presque	advPref	[pred="presque_____1",cat=adv]	presque_____1	Default 
presque	adjPref	[pred="presque_____1<Suj:(sn)>",cat=adj]	presque_____1	Default 

## should be handled by suffix
@adj russophone +s + +s
@adj n�erlandophone +s + +s
@adj lusophone +s + +s
@adj italophone +s + +s
@adj hispanophone +s + +s
@adj kurdophone +s + +s
@adj magyarophone +s + +s
@adj ukrainophone +s + +s

@nc agoniste +s @m

@redirect m'sieur monsieur

� c�t�	adv	[pred="� c�t�_____1",cat=adv]	� c�t�_____1	Default
� c�t�s	nc	[pred="� c�t�_____1",cat=nc,@mp]	� c�t�_____1	Default

�t�	v	[pred="�tre_____1<Suj:cln|scompl|sinf|sn,Obl:sinf>",@CtrlSujObl,@active,@pers,cat=v,@K]	�tre_____1	PastParticiple	K %actif

## tmp hack for 'avoir beau' : better to implement it as a concessive construction (il a beau manger, il ne grossit pas)
beau	100	cfi	[pred="beau_____1<Suj:cln|sn,Obl:sinf>",lightverb=avoir]	beau_____1	Default	%default

chance	100	cfi	[pred="chance_____1<Suj:cln|sn,Obj�:(�-sn)>",lightverb=porter]	chance_____1	Default	%default

##attention	100	cfi	[pred="attention_____1<Suj:cln|sn,Obj�:(�-sn|cld|�-sinf)>",lightverb=faire]	attention_____1	Default	%default
    
@nc rappeur +s @m
@nc rappeuse +s @f
@nc greffi�re +s @f

@nc parraineur +s @m

## should be done in lefff to be complete !
@redirect sous-estimer sousestimer
@redirect sous-estime sousestime
@redirect sous-estimes sousestimes
@redirect sous-estiment sousestiment
@redirect sous-estim� sousestim�
@redirect sous-estim�s sousestim�s
@redirect sous-estim�e sousestim�e
@redirect sous-estim�es sousestim�es

@nc semi-remorque +s @m
  
@adj record +s + +s

#voil�	120	prep	[pred="voil�_____1<Obj:(sn)>",pcas = tps]	voil�____1	Default	%default
#voici	120	prep	[pred="voici_____1",pcas= loc, cat= prep]	voici____1	Default	%default    

# Past Conditional seconde form
e�t	600	auxAvoir	[@active,@favoir,@C3s]	avoir_____2	ThirdSing	C3s	%default
e�tes	600	auxAvoir	[@active,@favoir,@C2p]	avoir_____2	Default	C2p	%default

commande	100	cfi	[pred="commande_____1<Suj:cln|sn,Obj�:(�-sn|cld),Objde:(de-sn|clg)>",lightverb=passer]	commande_____1	Default	%default

mati�re	100	cfi	[pred="mati�re_____1<Suj:cln|sn,Obj�:(�-sn|cld)>",lightverb=trouver]	mati�re_____1	Default	%default
pretexte	100	cfi	[pred="pretexte_____1<Suj:cln|sn,Obj�:(�-sn|cld)>",lightverb=trouver]	pretexte_____1	Default	%default
    
## Some acronymes
@nc PIB + @m
@nc TVA + @f
@nc RMI + @m
@nc TGV + @m
@nc PNB + @m
@nc PIB + @m
@nc BTP + @m
@nc RMiste +s @m
@nc TUC + @m
@nc PEL + @m

## irriter +obj

@nc contre-pouvoir +s @m

/	100	ponctw	[]	/_____1	Default	%default

## tmp hack
## du	100	prep	[pred="de_____1",pcas = de]	de_____1	Default	%default

@nc smic + @m
@nc SMIC + @m

@nc pdg + @m

diable	pres	[pred="diable_____1"]	diable_____1	Default

@nc �-pic + @m


conseil	100	cfi	[pred="conseil_____1<Suj:cln|sn>",lightverb=tenir]	conseil_____1	Default	%default
piti�	100	cfi	[pred="piti�_____1<Suj:cln|sn,Objde:(de-sn)>",lightverb=avoir]	piti�_____1	Default	%default

@adj siliceux siliceux siliceuse siliceuses



## corpus Pau
@nc toue +s @f
@nc �ra +s @f
@nc toulouho�re +s @f

## @nc rendez-vous + @m
